import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  uri = 'http://localhost:3000/user';


  constructor(private http: HttpClient) { }

  addUser(user_name, email, cell_no) {
    const obj = {
      user_name: user_name,
      email: email,
      cell_no: cell_no
    };
    console.log(obj);
    this.http.post(`${this.uri}/add`, obj)
      .subscribe(res => console.log('Done'));
  }

  getUser() {
    return this.http.get(`${this.uri}`);
  }

  editUser(id) {
    return this.http.get(`${this.uri}/edit/${id}`);
  }

  updateUser(user_name, email, cell_no, id) {
    const obj = {
      user_name: user_name,
      email: email,
      cell_no: cell_no
    };
    this.http.post(`${this.uri}/update/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }

  deleteUser(id) {
    return this
              .http
              .get(`${this.uri}/delete/${id}`);
  }
  
}

