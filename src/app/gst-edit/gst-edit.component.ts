import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../User.service';


@Component({
  selector: 'app-gst-edit',
  templateUrl: './gst-edit.component.html',
})


export class GstEditComponent implements OnInit {

  gst_editForm;
  user: any = {};

  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private router: Router, private us: UserService, private fb: FormBuilder) { this.createForm(); }

  createForm() {
    this.gst_editForm = this.fb.group({
      person_name: ['', Validators.required],
      email: ['', Validators.required],
      cell_no: ['', Validators.required]
    });
  }

  updateUser(person_name, business_name, business_gst_number) {
    this.route.params.subscribe(params => {
      this.us.updateUser(person_name, business_name, business_gst_number, params['id']);
      this.router.navigate(['user']);
    });
  }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.us.editUser(params['id']).subscribe(res => {
        this.user = res;
      });
    });
  }
}
