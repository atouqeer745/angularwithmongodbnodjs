import { Component, OnInit } from '@angular/core';
import { UserService } from '../User.service';
import User from '../User_model';

@Component({
  selector: 'app-gst-get',
  templateUrl: './gst-get.component.html',
})
export class GstGetComponent implements OnInit {
  users: User[];
  constructor(private us: UserService) { }

  deleteUser(id) {
    this.us.deleteUser(id).subscribe(res => {
      console.log('Deleted');
      this.ngOnInit();
    });
  }

  refresh() {
    location.reload();
  }
  ngOnInit() {
    this.us.getUser().subscribe((data: User[]) => {
      this.users = data;
    });
  }

}
