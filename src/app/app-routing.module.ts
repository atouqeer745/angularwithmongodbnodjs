import {NgModule} from '@angular/core';
import {Routes , RouterModule} from '@angular/router';

import { GstAddComponent } from './gst-add/gst-add.component';
import { GstEditComponent } from './gst-edit/gst-edit.component';
import { GstGetComponent } from './gst-get/gst-get.component';

const routes = [
    {
        path: 'user/create',
        component: GstAddComponent
    },
    {
        path: 'user',
        component: GstGetComponent
    } ,
    {
        path: 'user/edit/:id',
        component: GstEditComponent,

    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)] ,
    exports: [RouterModule]
})

export class AppRoutingModule {

}
