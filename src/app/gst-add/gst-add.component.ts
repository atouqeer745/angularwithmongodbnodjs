import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { UserService } from '../User.service';


@Component({
  selector: 'app-gst-add',
  templateUrl: './gst-add.component.html'
})
export class GstAddComponent implements OnInit {
  dataadded = false;
  msg: String ;
  gst_addForm;
  constructor(private fb: FormBuilder , private bs: UserService) {
    this.createForm();
  }

  createForm() {
    this.gst_addForm = this.fb.group({
      user_name: ['', Validators.required],
      email: ['', Validators.required],
      cell_no: ['', Validators.required]
    });
  }

  addUser (user_name, email, cell_no) {
  this.bs.addUser(user_name, email, cell_no);
  location.reload();
  this.dataadded = true;
  this.msg = 'Data Added successfully';
  }

  ngOnInit() {
  }

}
