const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/* Defining collection for User */ 
let User = new Schema({
    user_name: {
        type: String
    },
    email: {
        type: String
    },
    cell_no: {
        type: Number
    }
},
    {
        collection: 'User'
    });

    module.exports = mongoose.model('User', User);