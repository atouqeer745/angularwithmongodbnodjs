const express = require('express');
const User = require('../models/User/User');

const app = express();
const userRoutes = express.Router();

/* Require User models here */

// Adding new data

userRoutes.route('/add').post((req, res) => {
    let user = new User(req.body);
    user.save().then(user => {
        res.status(200).json({ 'user': 'user is added successfully' });
    })
        .catch(err => {
            res.status(400).send('Unable to add  user to database')
        });
});

//Getting all stored data

userRoutes.route('/').get((req, res) => {
    User.find(function (err, business) {
        if (business) {
            res.json(business);
        } else {
            console.log(err);
        }
    });
});

// Editing the data 
userRoutes.route('/edit/:id').get((req, res) => {
    let id = req.params.id;
    User.findById(id, (err, user) => {
        res.json(user);
    });
});

// Updating the data

userRoutes.route('/update/:id').post((req, res) => {
    User.findById(req.params.id, (err, user) => {
        if (!user) {
            return next(new Error('Could not load the document'));
        } else {
            user.user_name = req.body.user_name;
            user.email = req.body.email;
            user.cell_no = req.body.cell_no;

            user.save().then(business => {
                res.json('Data Updated Successfully');
            })
                .catch(err => {
                    res.status(400).send('Unable to update the database');
                });
        }
    });
});

// Deleting the data

userRoutes.route('/delete/:id').get((req, res) => {
    let id = req.params.id;
    User.findByIdAndDelete(id, (err, business) => {
        if (err) {
            res.json(err)
        } else {
            res.json('Data Removed Successfully');
        }
    });
});

module.exports = userRoutes;